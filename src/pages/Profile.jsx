import React from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Row, Button } from "antd";
import { Descriptions, Badge } from 'antd';

@inject('authStore')
@withRouter
@observer
export default class Profile extends React.Component {
  render() {
    const { currentUser } = this.props.authStore;
    return (
      <React.Fragment>
        <Row style={{ background: "#FFF", marginTop: 5, padding: 10 }}>
          <Descriptions title="账户信息" bordered>
            <Descriptions.Item label="账户名" span={3}>{currentUser.username}</Descriptions.Item>
            <Descriptions.Item label="角色" span={3}>{currentUser.role}</Descriptions.Item>
            <Descriptions.Item label="邮箱" span={3}>{currentUser.email}</Descriptions.Item>
            <Descriptions.Item label="联系方式" span={3}>{currentUser.phoneNumber}</Descriptions.Item>
            <Descriptions.Item label="创建时间" span={3}>{currentUser.createTime}</Descriptions.Item>
            <Descriptions.Item label="个人简介">
              Data disk type: MongoDB
              <br />
              Database version: 3.4
              <br />
              Package: dds.mongo.mid
              <br />
              Storage space: 10 GB
              <br />
              Replication_factor:3
              <br />
              Region: East China 1<br />
            </Descriptions.Item>
          </Descriptions>
        </Row>
        <Row type="flex" justify="end">
            <Button className="float-right" onClick={() => this.props.authStore.logout()}>
              登出
            </Button>
        </Row>
      </React.Fragment>
    );
  }
}