import React from "react";
import { inject, observer } from "mobx-react";
import { Switch, Route, withRouter, Link } from "react-router-dom";
import {
  Input,
  Descriptions,
  List,
  Row,
  AutoComplete,
  Button,
  Form,
  DatePicker,
  Spin
} from "antd";

import moment from "moment";

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 3 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 21 }
  }
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0
    },
    sm: {
      span: 16,
      offset: 12
    }
  }
};

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

@Form.create({ name: "user" })
@inject("usersStore")
@withRouter
@observer
export default class Account_signin extends React.Component {
  state = {
    validation: false
  };
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.usersStore.addUser();
      }
    });
  };

  render() {
    const { usersStore } = this.props;
    const {
      getFieldDecorator,
      getFieldsError,
      getFieldError,
      isFieldTouched
    } = this.props.form;
    return (
      <React.Fragment>
        <Row style={{ background: "#FFF", marginTop: 5, padding: 10 }}>
          <Spin spinning={usersStore.isLoading}>
          <Form {...formItemLayout} onSubmit={this.handleSubmit}>
            <Form.Item
              label="注册邮箱"
              validateStatus={
                isFieldTouched("userId") && getFieldError("userId")
                  ? "error"
                  : ""
              }
              help={(isFieldTouched("userId") && getFieldError("userId")) || ""}
            >
              {getFieldDecorator("userId", {
                rules: [
                  {
                    required: true,
                    message: "请输入注册邮箱"
                  }
                ]
              })(
                <Input
                  size="large"
                  id="userId"
                  value={usersStore.user.userId}
                  onChange={e =>
                    usersStore.updateUser(
                      e.currentTarget.id,
                      e.currentTarget.value
                    )
                  }
                />
              )}
            </Form.Item>
            <Form.Item
              label="登录密码"
              validateStatus={
                isFieldTouched("password") && getFieldError("password")
                  ? "error"
                  : ""
              }
              help={
                (isFieldTouched("password") && getFieldError("password")) || ""
              }
            >
              {getFieldDecorator("password", {
                rules: [
                  {
                    required: true,
                    message: "请输入密码"
                  }
                ]
              })(
                <Input
                  size="large"
                  id="password"
                  value={usersStore.user.password}
                  onChange={e =>
                    usersStore.updateUser(
                      e.currentTarget.id,
                      e.currentTarget.value
                    )
                  }
                />
              )}
            </Form.Item>
            <Form.Item
              label="用户昵称"
              validateStatus={
                isFieldTouched("nickname") && getFieldError("nickname")
                  ? "error"
                  : ""
              }
              help={
                (isFieldTouched("nickname") && getFieldError("nickname")) || ""
              }
            >
              {getFieldDecorator("nickname", {
                rules: [
                  {
                    required: true,
                    message: "请输入用户昵称"
                  }
                ]
              })(
                <Input
                  size="large"
                  id="nickname"
                  value={usersStore.user.nickname}
                  onChange={e =>
                    usersStore.updateUser(
                      e.currentTarget.id,
                      e.currentTarget.value
                    )
                  }
                />
              )}
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" disabled={hasErrors(getFieldsError())}>
                注册
              </Button>
            </Form.Item>
          </Form>
          </Spin>
        </Row>
      </React.Fragment>
    );
  }
}