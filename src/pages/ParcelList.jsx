import React from "react";
import { inject, observer } from "mobx-react";
import { Switch, Route, withRouter, Link } from "react-router-dom";
import {
  Row,
  Layout,
  Table,
  Breadcrumb,
  Input,
  Descriptions,
  List,
  Button,
  Icon,
  Empty,
  Spin,
  Pagination
} from "antd";

const { Header, Content, Footer } = Layout;
const { Search } = Input;

@inject('parcelsStore')
@withRouter
@observer
export default class ParcelList extends React.Component {
  changePage = (page, perPage) => {
    // console.log(page, perPage)
    this.props.parcelsStore.getParcels(page);
  };

    render(){
        const { parcelsStore } = this.props
        console.log(parcelsStore)
        return(
            <React.Fragment>
                <Row style={{background:'#FFF', marginTop:5, padding: 10}}>
                    <Search placeholder="Search using Express ID ..." 
                        autoFocus={true}
                        size='large' 
                        allowClear
                        style={{ width: '100%' }}
                        onSearch={(value) => parcelsStore.queryRecord(value)} 
                        enterButton />
                    <Button type="link" onClick={() => parcelsStore.getParcels(1)}>Show All</Button>
                </Row>
                <Row style={{background:'#FFF', marginTop:5, padding: 10, minHeight:60}}>
                    <Table 
                        columns={parcelsStore.columns} 
                        dataSource={parcelsStore.currentPageParcels} 
                        loading={parcelsStore.isLoading}
                        pagination={{ defaultCurrent:1, total:parcelsStore.total, onChange: this.changePage, pageSize: parcelsStore.perPage}} />
                </Row>
            </React.Fragment>
        )
    }
}
