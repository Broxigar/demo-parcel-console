import React from "react";
import { inject, observer } from "mobx-react";
import { Switch, Route, withRouter, Link } from "react-router-dom";
import {
  Layout,
  Menu,
  Breadcrumb,
  Icon,
  Dropdown,
  Button,
  Row,
  Col
} from "antd";

import './MainFrame.css'
import AddParcel from "./AddParcel";
import Price from "./Price";
import ParcelList from "./ParcelList";
import UploadParcels from './UploadParcels';
import Login from "./Login";
import Profile from "./Profile";


const { Header, Sider, Content, Footer } = Layout;

@inject("historyStore")
@withRouter
@observer
export default class MainFrame extends React.Component {
  state = {
    collapsed: false
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };
                //     </Menu>
                // </Sider>
                // <Layout>
                // <Header style={{ background: '#fff', padding: 0 }}>
                //     <Icon
                //         className="trigger"
                //         type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                //         onClick={this.toggle}
                //         />
                //     {/* <Menu
                //         theme="dark"
                //         mode="horizontal"
                //         defaultSelectedKeys={['1']}
                //         style={{ lineHeight: '48px' }}
                //     >
                //         <Menu.Item key="1"><Link to='/'>物流查询</Link></Menu.Item>
                //         <Menu.Item key="2"><Link to='/price'>价格查询</Link></Menu.Item>
                //     </Menu> */}
                //     </Header>
                //     <Content  style={{
                //         margin: '24px 16px',
                //         padding: 24,
                //         background: '#fff',
                //         minHeight: 280,
                //         }}>
                //         {/* <Breadcrumb style={{ margin: '16px 0' }}>
                //             <Breadcrumb.Item>Home</Breadcrumb.Item>
                //             <Breadcrumb.Item>List</Breadcrumb.Item>
                //             <Breadcrumb.Item>App</Breadcrumb.Item>
                //         </Breadcrumb> */}
                //         <Switch>
                //             <Route exact path='/' component={ParcelList} />
                //             <Route exact path='/addParcel' component={AddParcel} />
                //             <Route exact path='/uploadParcels' component={UploadParcels} />

  render() {
    return (
      <Layout>
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          className="sidebar"
        >
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item key="1">
              <Link to="/">
                <Icon type="unordered-list" />
                <span>所有包裹</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/addParcel">
                <Icon type="file-add" />
                <span>添加包裹</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/uploadParcels">
                <Icon type="upload" />
                <span>批量上传</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: "#fff", padding: 0 }}>
            <Row type="flex" justify="space-between">
              <Col>
                <Icon
                  className="trigger"
                  type={this.state.collapsed ? "menu-unfold" : "menu-fold"}
                  onClick={this.toggle}
                />
              </Col>
              <Col>
                <Icon type="user" className="user-icon" onClick={()=>this.props.history.push('/profile')}/>
                {/* <Button className="user-icon" >
                  <Link to="/profile">
                    
                  </Link>
                </Button> */}
              </Col>
            </Row>
          </Header>
          <Content
            style={{
              margin: "24px 16px",
              padding: 24,
              background: "#fff",
              minHeight: 280
            }}
          >
            <Switch>
              <Route exact path="/" component={ParcelList} />
              <Route exact path="/addParcel" component={AddParcel} />
              <Route exact path="/uploadParcels" component={UploadParcels} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/profile" component={Profile} />
            </Switch>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            物流 ©2019 Created by InstSoft
          </Footer>
        </Layout>
      </Layout>
    );
  }
}
