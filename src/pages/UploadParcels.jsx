import React from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { Spin, Row, Table, Button, upload, Alert} from 'antd';
import template from '../resource/template.jpg'
import './UploadParcels.css'

const Dragger = upload.Dragger;

@inject('parcelsStore')
@withRouter
@observer
export default class UploadParcels extends React.Component {
    
    render(){
        const { parcelsStore } = this.props
        const columnsWithOperation = parcelsStore.excelColumns.length === 0 ? [] : parcelsStore.excelColumns.slice().concat([
            {
                title: '状态',
                dataIndex: 'status',
                render: (text, record, index) => <div>{
                    record.status==='success'? <Alert message="Success" type="success" showIcon /> : 
                    record.status==='uploading' ? <Alert message="Uploading" type="info" showIcon />: 
                    record.status==='failed' ? <Alert message="Error" type="error" showIcon /> : ''
                }</div>
            },
        ])

        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
            //   console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows)
                parcelsStore.selectedParcels(selectedRowKeys, selectedRows)
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User', // Column configuration not to be checked
                name: record.name,
            }),
        };

        // const rowClassName = (record, index) => {
        //     let className = ""
        //     if (record.status == "uploading")
        //         className = "uploading"
        //     else if (record.status == "success")
        //         className = "success"
        //     else if (record.status == "failed")
        //         className = "failed"
        //     return className
        // }

        return(
            <React.Fragment>
                <Spin spinning={parcelsStore.isLoading}>
                <Dragger name="file"
                    beforeUpload={function () {
                    return false;
                    }}
                    onChange={(file) => parcelsStore.uploadParcels(file)}
                    showUploadList={false}>
                    <p className="ant-upload-text">
                        <span>点击上传文件或者拖拽上传</span>
                    </p>
                </Dragger>
                <Row style={{background:'#FFF', marginTop:5, padding: 10, minHeight:60}}>
                    <Table 
                        columns = {columnsWithOperation} 
                        dataSource = {parcelsStore.excelParcels.slice()}
                        // rowClassName = {rowClassName}
                        rowSelection = {rowSelection}
                        pagination={false}
                    />
                </Row>
                <Row style={{background:'#FFF', marginTop:5, padding: 10}} type='flex' justify='start'>
                    <h4>注意:</h4>
                    <p>请记录下上传失败的订单号，并手动通过“添加包裹”栏逐一添加. </p>
                    <p>批量上传程序严格按照预定义excel文件解析文件，excel文件中任何不规范数据或者错误格式数据都会导致数据插入失败.</p>
                </Row>
                <Row style={{background:'#FFF', marginTop:5, padding: 10}} type='flex' justify='end'>
                    <Button type="primary" onClick={() => parcelsStore.addParcels()}>添加订单</Button>
                </Row>
                <Row style={{background:'#FFF', marginTop:5, padding: 10}}>
                    <h4>注意:</h4>
                    <p>请上传 <b>.xlsx</b> 文件，并把Excel第一个Sheet名修改为 <b>Sheet1</b>, 并且Excel文件中的列必须与下图保持一致: </p>
                    <p><img src={template} alt="Price" style={{width: '100%'}} /></p>
                </Row>
                </Spin>
            </React.Fragment>
        )
    }

}