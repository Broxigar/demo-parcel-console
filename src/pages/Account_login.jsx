import React from "react";
import { inject, observer } from "mobx-react";
import { Switch, Route, withRouter, Link } from "react-router-dom";
import { Layout, Menu, Breadcrumb, Row, Form, Input, Button } from "antd";

const { Header, Content, Footer } = Layout;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 3 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 21 }
  }
};
//@inject('usersStore')
@withRouter
@observer
export default class Account_login extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Row style={{ background: "#FFF", marginTop: 5, padding: 10 }}>
          <Form {...formItemLayout}>
            <Form.Item label="邮箱">{<Input size="large" />}</Form.Item>
            <Form.Item label="密码">{<Input size="large" />}</Form.Item>
          </Form>
        </Row>
        <Row type="flex" justify="end">
            <Button className="float-right">登录</Button>
        </Row>
      </React.Fragment>
    );
  }
}
