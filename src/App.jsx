import React from 'react';
import { inject, observer } from "mobx-react";
import { withRouter } from 'react-router-dom';
import MainFrame from './pages/MainFrame'
import Login from './pages/Login'
import './App.css';


@inject('authStore')
@withRouter
@observer
export default class App extends React.Component {
  render(){
    if(!this.props.authStore.currentUser.username) return(<MainFrame />)
    return(
      <MainFrame />
    )
  }
}

