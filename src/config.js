const PROD_API_URL = {
    ROOT_URL: '',
    ROOT_API_URL: '/api',
    ROOT_QAPI_URL: '/qapi',
}
const DEV_API_URL = {
    ROOT_URL: 'http://localhost:3000',
    ROOT_API_URL: 'http://localhost:3000/api',
    ROOT_QAPI_URL: 'http://localhost:3000/qapi',
    ROOT_KION_URL: 'http://localhost:3306/qapi',
}
let API_URL = PROD_API_URL
process.env.REACT_APP_MODE==='dev' ? API_URL = DEV_API_URL : API_URL = PROD_API_URL

// console.log(process.env, process.env.REACT_APP_MODE, API_URL)
// console.log(process.env.REACT_APP_TEST)

export default API_URL
