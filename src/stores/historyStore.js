import { observable, action, computed, reaction, createTransformer } from 'mobx';
import API_URL from '../config'

class HistoryStore{
    history = observable.map()

    @action addHistory(expressId, records){
        this.history.set(expressId, records)
    }
}

export default new HistoryStore()