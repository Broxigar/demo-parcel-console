import { observable, action, computed, createTransformer,runInAction } from 'mobx';
import { message } from 'antd';

import API_URL from '../config'
import stores from './index'
import * as XLSX from 'xlsx';

class Parcel {
    orderId = ''
    parcelId = ''
    expressName = ''
    expressId = ''
    length = 0.0
    width = 0.0
    height = 0.0
    volume = 0.0
    weight = 0.0
    packing = ''
    store = ''
    storeComment = ''
    storeTime = ''
    oldParcelId = ''
    status = ''
}

class ParcelsStore {

    columns = [
        {
            title: '订单号',
            dataIndex: 'orderId',
        },
        {
            title: '包号',
            dataIndex: 'parcelId',
        },
        {
            title: '快递公司',
            dataIndex: 'expressName',
        },
        {
            title: '快递单号',
            dataIndex: 'expressId',
        },
        {
            title: '长度(CM)',
            dataIndex: 'length',
        },
        {
            title: '宽度(CM)',
            dataIndex: 'width',
        },
        {
            title: '高度(CM)',
            dataIndex: 'height',
        },
        {
            title: '体积(立方米)',
            dataIndex: 'volume',
        },
        {
            title: '重量(KG)',
            dataIndex: 'weight',
        },
        {
            title: '包装',
            dataIndex: 'packing',
        },
        {
            title: '仓位',
            dataIndex: 'store',
        },
        {
            title: '入库备注',
            dataIndex: 'storeComment',
        },
        {
            title: '入库时间',
            dataIndex: 'storeTime',
        },
        {
            title: '旧包号',
            dataIndex: 'oldParcelId',
        },
    ]

    @observable isLoading = false
    @observable total = 0
    @observable perPage = 20
    @observable parcelsInPage = observable.map()
    @observable currentPageParcels = []
    @observable excelColumns = []
    @observable excelParcels = []
    @observable parcels = []
    @observable parcel = new Parcel()
    
    constructor(){
        this.getParcels(1)
        this.getTotal()
    }
    @action getParcels(page){
        let adjustedPage = page-1
        this.getTotal()
        const cache = this.parcelsInPage.get(adjustedPage)
        if(cache){
            this.currentPageParcels = cache
        }else{
            this.isLoading = true
            fetch(`${API_URL.ROOT_QAPI_URL}/getParcelsByPage/${adjustedPage*this.perPage}/${this.perPage}`)
            .then(action(res=>{
                if(res.status === 200){
                    return res.json()
                }else{
                    stores.systemStore.networkError = true
                    stores.systemStore.networkErrorInfo = res
                }
            }))
            .then(action(json => {
                this.parcelsInPage.set(adjustedPage, json)
                this.currentPageParcels = json
            })).finally(action(()=>this.isLoading=false))
        }
    }
    @action uploadParcels(file) {
        // console.log(file)
        const fileReader = new FileReader();
        fileReader.onload = event => {
            try {
                // console.log(event)
                const {result} = event.target;
                const workbook = XLSX.read(result, {type: 'binary'});
                let data = {};
                // console.log(workbook.Sheets)
                for (const sheet in workbook.Sheets) {
                    let tempData = [];
                    if (workbook.Sheets.hasOwnProperty(sheet)) {
                        data[sheet] = tempData.concat(XLSX.utils.sheet_to_json(workbook.Sheets[sheet]));
                    }
                }
            
                const excelSheet = data.Sheet1
                const excelHeader = []
                const excelData = []

                for (const headerAttr in excelSheet[0]) {
                    const header = {
                        title: headerAttr,
                        dataIndex: this.columns.find(item=>item.title==headerAttr).dataIndex,
                        key: this.columns.find(item=>item.title==headerAttr).dataIndex
                    }
                    excelHeader.push(header)
                }
                
                excelSheet.forEach(row => {
                    let parcel = new Parcel()
                    for (const k in row) {
                        let id = this.columns.find(item=>item.title==k).dataIndex
                        parcel[id] = row[k]
                    }
                    excelData.push(parcel)
                })
            
                message.success('上传成功！')
                
                this.excelColumns = excelHeader
                this.excelParcels = excelData

            } catch (e) {
                console.log(e);
                message.error('文件类型不正确！');
            }
        }

        fileReader.readAsBinaryString(file.file);
    }
    @action updateParcel(id, value){
        const key = id.split('_')[1]
        this.parcel[key] = value
    }
    @action selectedParcels(keys, rows) {
        this.parcels = rows
    }
    @action addParcels() { 
        
        for(let parcel of this.parcels) {
            this.addParcel(parcel)
        }
        
    }
    @action addParcel(parcel = this.parcel){
        this.isLoading = true
        parcel.status = "uploading"

        let data = Object.assign({}, parcel)
        delete data["status"]

        fetch(`${API_URL.ROOT_QAPI_URL}/addParcel`, {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(action(res=>{
            if(res.status === 200){
                runInAction(() =>parcel.status = "success")
                this.parcel = new Parcel()
                this.getParcels(1)
                this.getTotal()
                // window.alert('添加订单成功，点击确定继续添加')
            }else{
                parcel.status = "failed"
                stores.systemStore.networkError = true
                stores.systemStore.networkErrorInfo = res
                console.log('Error: ', res)
                // window.alert('添加订单失败，请查看错误日志以解决问题')
            }
        }))
        .finally(action(()=>this.isLoading=false))
    }
    @action queryRecord(expressId){
        if((''+expressId).trim().length === 0) return
        let adjustedExpressId = expressId.trim()
        this.isLoading = true
        fetch(`${API_URL.ROOT_QAPI_URL}/queryByExpressId/${adjustedExpressId}`)
        .then(action(res=>{
            if(res.status === 200){
                return res.json()
            }else{
                stores.systemStore.networkError = true
                stores.systemStore.networkErrorInfo = res
            }
        }))
        .then(action(json => {
            if(json.length > 0){
                this.currentPageParcels = json
                this.total = json.length
                this.perPage = json.length
            }else{
                this.currentPageParcels = []
                this.total = 0
            }
        })).finally(action(()=>this.isLoading=false))  
    }


    @action getTotal(){
        this.perPage = 20
        fetch(`${API_URL.ROOT_QAPI_URL}/getTotalOrders`)
            .then(res=>res.json())
            .then(action(json => {
                this.total = json[0].total
            }))
    }
}

export default new ParcelsStore()