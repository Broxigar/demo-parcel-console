import { observable, action, computed, createTransformer } from 'mobx';

import API_URL from '../config'
import stores from './index'

class QueryStore{
    @observable isLoading = false
    @observable expressId = ''
    @observable queriedRecords = []
    @observable emptyResult = false

    @action queryRecord(expressId){
        this.isLoading = true
        this.emptyResult = false
        this.expressId = expressId
        // Note: if shallow copy of array, .clear() here will clear the cached array
        this.queriedRecords.clear()
        let cache =  stores.historyStore.history.get(expressId)
        if(cache && cache.length>0){ 
            // Note: use Array.from to deep copy array from cached
            this.queriedRecords = Array.from(cache)
            this.isLoading=false
        }else{
            fetch(`${API_URL.ROOT_KION_URL}/queryByExpressId/${expressId}`)
            .then(action(res=>{
                if(res.status === 200){
                    return res.json()
                }else{
                    stores.systemStore.networkError = true
                    stores.systemStore.networkErrorInfo = res
                }
            }))
            .then(action(json => {
                console.log(json)
                if(json.length > 0){
                    this.queriedRecords = json
                    stores.historyStore.addHistory(expressId, Array.from(json))
                }else{
                    this.emptyResult = true
                }  
            })).finally(action(()=>this.isLoading=false))
        }
    }
}

export default new QueryStore()